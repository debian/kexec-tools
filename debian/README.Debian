systemd:

The "reboot" command with systemd will by default do a cold reboot. To
kexec a new kernel with systemd, use "systemctl kexec". For this to
work, you need to have a kernel loaded into kexec first. This can be
done (a) manually:

# kexec -l /boot/vmlinuz-XXX-YYY --initrd=/boot/initrd.img-XXX-YYY --reuse-cmdline

or (b) by using a boot loader which tells systemd which kernel is
loaded. The systemd-boot boot loader does this. Grub does not, at
least not yet.

sysvinit:

This package puts two scripts in /etc/init.d/ — kexec-load and kexec.
Under sysvinit, kexec-load is called upon reboot to load the kernel
into kexec, if kexec is enabled in /etc/default/kexec. The kexec
script is called just before the reboot script. It checks if kexec is
enabled and a kexec'able kernel is loaded, and if so, kexecs a new
kernel instead of rebooting. As a result, kexec is automatically
enabled upon installation of kexec-tools package if kexec is enabled
in /etc/default/kexec through the debconf option.

warnings:

Apparently kexec has some issues with GPU driver modules, which need
to be unloaded first of the device might be unusable by the new
kernel. It also has issues with some filesystems which may become
corrupted under certain circumstanecs. See bugs.debian.org/kexec-tools
for tracking and details.

 -- Barak A. Pearlmutter <bap@debian.org>, Sat, 16 Sep 2023 21:01:36 +0100
